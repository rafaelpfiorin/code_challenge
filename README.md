# QA technical test

## How to use it

1. Install Python 3.11 or later.
2. Clone this project with HTTPS option.
3. Open the prompt and install requests package using: pip install requests.
4. After compiling and executing 'main.py' file, the generated reports can be found into the 'report' directory (*.txt file).

Enjoy!
