# dependencia para requsicoes em API
import requests
# dependencia para criar diretorio
import os


class User:
    # construtor da entidade de Usuario - passo 1
    def __init__(self, nm, p_url, p_rps, flws, flwg):
        self.name = nm
        self.html_url = p_url
        self.public_repos = p_rps
        self.followers = flws
        self.following = flwg


class UserService:
    def get_user(self, u_nm):
        # chama endpoint de usuarios do github, interpolando parametro
        gh_url = f"https://api.github.com/users/{u_nm}"
        response = requests.get(gh_url)

        if (response.status_code == 200):
            # se obtiver sucesso, popula as variaveis com o body retornado
            data = response.json()

            if (data["name"] is None):
                # trata casos em que o nome esta sem valor
                name = "null"
                html_url = data["html_url"]
                public_repos = data["public_repos"]
                followers = data["followers"]
                following = data["following"]
            else:
                name = data["name"]
                html_url = data["html_url"]
                public_repos = data["public_repos"]
                followers = data["followers"]
                following = data["following"]

            # cria instancia de User passando o conteudo da requisicao
            return User(name, html_url, public_repos, followers, following)
        else:
            return None

    def get_user_repos(self, u_nm) -> dict:
        # chama endpoint de repositorios, interpolando nome de usuario
        gh_url = f"https://api.github.com/users/{u_nm}/repos"
        response = requests.get(gh_url)

        if (response.status_code == 200):
            dictionary = dict()
            # se obtiver sucesso, popula o dicionario com body retornado
            data = response.json()

            for repo in data:
                name = repo["name"]
                url = repo["html_url"]
                dictionary[name] = url

            return dictionary
        else:
            # caso falhe requisicao, nao popula
            return None

    def user_report(self, user: User, u_nm: str, repos: dict) -> None:
        # cria pasta e arquivo
        fdr = "report/"
        if (not os.path.exists(fdr)):
            os.mkdir(fdr)

        file = open(fdr+u_nm+'.txt', 'w')

        # escreve os dados solicitados
        file.write("Nome: "+user.name+"\n")
        file.write("URL do perfil: "+user.html_url+"\n")
        file.write("Nº de repositórios: "+str(user.public_repos)+"\n")
        file.write("Nº de seguidores: "+str(user.followers)+"\n")
        file.write("Nº de usuários que segue: "+str(user.following)+"\n")
        file.write("Repositórios: "+"\n")
        for val in repos:
            file.write("\t"+val+': '+repos[val]+"\n")

        # encerra a escrita do texto
        file.close()
