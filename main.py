# inclui arquivo 'user.py'
import user
# dependencia para t. unitario
import unittest

if __name__ == "__main__":
    # instancia classe de servico da entidade User e solicita username do github
    service = user.UserService()
    u_nm = input(
        "Digite o nome de usuário do github que deseja gerar o relatório: ")
    # dispara metodo de requisicao do passo 2
    usr = service.get_user(u_nm)
    # dispara metodo de requisicao do passo 3
    dty = service.get_user_repos(u_nm)
    # exibe os repositorios do usuario digitado
    print(dty)

    # dispara metodo do passo 4
    service.user_report(usr, u_nm, dty)

    class TestMethods(unittest.TestCase):
        """ Classe de testes unitarios - ultimo passo """

        def test_user_class_has_minimal_parameters(self):
            """
                Teste unitario relativo ao primeiro passo do desafio, esse cenario deve ser mantido na sua resolucao.
                Aqui busca-se na API dados do usuario testado e por meio de um array, compara as chaves retornadas
            """
            svc = user.UserService()

            u = service.get_user('githubuser')

            parameters = [
                'name', 'html_url', 'public_repos', 'followers', 'following'
            ]

            for param in parameters:
                self.assertTrue(hasattr(u, param))

        def test_get_invalid_user(self):
            """ Teste unitario relativo a tentativa de buscar um usuario que nao existe """
            svc = user.UserService()
            u = service.get_user('invalid_github-user')

            self.assertIsNone(u)

        def test_get_user_repos(self):
            """
                Teste unitario relativo ao terceiro passo do desafio.
                Aqui busca-se na API dados do usuario testado verificando se retorna um dicionario e se nao esta vazio
            """
            svc = user.UserService()
            rp = service.get_user_repos('githubuser')

            self.assertIsInstance(rp, dict)
            self.assertGreater(len(rp), 0)

        def test_user_report(self):
            """
                Teste unitario relativo ao ultimo passo do desafio.
                Aqui busca-se na API dados do usuario escolhido, grava-os no arquivo, faz a leitura e verifica se os dados do usuario e seus repositorios possuem conteudo.
                Escolha o nome de usuario do github na variavel 'nm'
            """
            svc = user.UserService()
            nm = 'githubuser'

            u = service.get_user(nm)
            rp = service.get_user_repos(nm)

            service.user_report(u, nm, rp)

            file = f'report/{nm}.txt'

            with open(file, "r") as f:
                content = f.read()

                self.assertIn(u.name, content)
                self.assertIn(u.html_url, content)
                self.assertIn(str(u.public_repos), content)
                self.assertIn(str(u.followers), content)
                self.assertIn(str(u.following), content)

                for nm, url in rp.items():
                    self.assertIsInstance(nm, str)
                    self.assertIsInstance(url, str)

    # executa os testes
    unittest.main()
